# dotFiles

playing around with dot files for linux command line environment

### How to use code
1. make sure you are working on a linux machine
2. type `cd ~`
3. type `git clone https://github.com/quinnliu/dotFiles.git`
4. type `cp dotFiles/.bashrc .bashrc`
5. type `cp dotFiles/.bash_profile .bash_profile`
6. type `source ./.bash_profile`

Now your command line prompt should like something similar to the following:
```sh
quinnliu@sumac:~ > # your next command here
```
