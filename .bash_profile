# Get the aliases and functions from local config file:
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

#
# Set path here:
smile() {
    if [ $? -ne 0 ]; then
	echo ":-("
    else
	echo ":-)"
    fi
}

export PATH=$PATH:$HOME/bin:$HOME/tmp:$HOME/and
export ROS_PACKAGE_PATH=/home/q/catkin_ws/src:/opt/ros/kinetic/share
# Set prompt here:
PS1='\n$(smile)\nq@\h:\w> '

alias ll="ls -lahG"

#
# Set default file permissions here:

# end .bash_profile
